/*
 * Something of this project.
 */
/**
 * @since 1.0
 * @author Shuhai Vladyslav
 * @version 1.0
 */

package com.shuhai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
/**
 * @since 1.0
 * @author Shuhai Vladyslav
 * @version 1.0
 */
final class FibonacciNumber {
    /**
     * This is the main method which makes use of enterInterval method.
     *
     * @param args Unused.
     */
    public static void main(final String[] args) {
        enterInterval();
    }

    /**
     * Constructor.
     */
    private FibonacciNumber() {
    }

    /**
     * This is the method which allows the user to enter
     * an interval and print all tasks.
     */
    private static void enterInterval() {
        final int start, end, fibonacciSize;
        List<Integer> fibonacciNumbers;
        Scanner scanner = new Scanner(System.in, "UTF-8");
        System.out.println("Input interval: [a;b]");
        System.out.print("a = ");
        start = scanner.nextInt();
        System.out.print("b = ");
        end = scanner.nextInt();
        System.out.println("Odd numbers:" + printOddNumbers(start, end));
        System.out.println("Even numbers:" + printEvenNumbers(start, end));
        System.out.println("Summary of odd numbers:"
                + sumOddNumbers(printOddNumbers(start, end)));
        System.out.println("Summary of even numbers:"
                + sumEvenNumbers(printEvenNumbers(start, end)));
        System.out.print("Enter size of fibonacci set:");
        fibonacciSize = scanner.nextInt();
        fibonacciNumbers = fibonacciNumbers(
                findTheBiggestElementInList(printOddNumbers(start, end)),
                findTheBiggestElementInList(printEvenNumbers(start, end)),
                fibonacciSize);
        System.out.println(fibonacciNumbers);
        evenAndOddFibonacciNumbers(fibonacciNumbers);
    }

    /**
     * This is method return odd numbers.
     *
     * @param start start interval
     * @param end   end interval
     * @return odd numbers
     */
    private static List<Integer> printOddNumbers(final int start,
                                                 final int end) {
        List<Integer> oddNumbers = new ArrayList<Integer>();
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                oddNumbers.add(i);
            }
        }
        return oddNumbers;
    }

    /**
     * This is method return even numbers.
     *
     * @param start start interval
     * @param end   end interval
     * @return even numbers
     */
    private static List<Integer> printEvenNumbers(final int start,
                                                  final int end) {
        List<Integer> evenNumbers = new ArrayList<Integer>();
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                evenNumbers.add(i);
            }
        }

        return evenNumbers;
    }

    /**
     * This is method return sum of the odd numbers.
     *
     * @param oddNumbers odd numbers
     * @return sum of the odd numbers
     */
    private static int sumOddNumbers(final List<Integer> oddNumbers) {
        int sum = 0;
        for (Integer oddNumber : oddNumbers) {
            sum += oddNumber;
        }
        return sum;
    }

    /**
     * This is method return sum of the even numbers.
     *
     * @param evenNumbers even numbers
     * @return sum of the even numbers
     */
    private static int sumEvenNumbers(final List<Integer> evenNumbers) {
        int sum = 0;
        for (Integer evenNumber : evenNumbers) {
            sum += evenNumber;
        }
        return sum;
    }

    /**
     * This method find the biggest element in any list.
     *
     * @param numbers list of numbers
     * @return the biggest element
     */
    private static int findTheBiggestElementInList(
            final List<Integer> numbers) {
        return Collections.max(numbers);
    }

    /**
     * This method build Fibonacci range.
     *
     * @param f1   the first number in range
     * @param f2   the second number in range
     * @param size size of the range
     * @return Fibonacci range
     */
    private static List<Integer> fibonacciNumbers(final int f1,
                                                  final int f2,
                                                  final int size) {
        List<Integer> fibonacciNumbers = new ArrayList<Integer>();

        fibonacciNumbers.add(f1);
        fibonacciNumbers.add(f2);
        for (int i = 0; i < size - 1; i++) {
            fibonacciNumbers.add(fibonacciNumbers.get(i)
                    + fibonacciNumbers.get(i + 1));
        }

        return fibonacciNumbers;
    }

    /**
     * This is method find even and odd Fibonacci numbers and print them.
     *
     * @param fibonacciNumbers Fibonacci list
     */
    private static void evenAndOddFibonacciNumbers(
            final List<Integer> fibonacciNumbers) {
        List<Integer> evenFibonacciNumbers = new ArrayList<Integer>();
        List<Integer> oddFibonacciNumbers = new ArrayList<Integer>();
        int percentageOddNum;
        int percentageEvenNum;
        final int percentCoef = 100;
        for (Integer fibonacciNumber : fibonacciNumbers) {
            if (fibonacciNumber % 2 == 0) {
                evenFibonacciNumbers.add(fibonacciNumber);
            } else {
                oddFibonacciNumbers.add(fibonacciNumber);
            }
        }
        percentageEvenNum = percentCoef * evenFibonacciNumbers.size()
                / fibonacciNumbers.size();
        percentageOddNum = percentCoef * oddFibonacciNumbers.size()
                / fibonacciNumbers.size();
        System.out.println("Even fibonacci numbers:" + evenFibonacciNumbers);
        System.out.println("Odd fibonacci numbers:" + oddFibonacciNumbers);
        System.out.println("Percentage of odd fibonacci number:"
                + percentageOddNum + "%");
        System.out.println("Percentage of even fibonacci number:"
                + percentageEvenNum + "%");
    }
}
